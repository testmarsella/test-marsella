package com.test.marsella.dao;

import java.util.ArrayList;
import java.util.List;

import com.test.marsella.model.User;

import org.springframework.stereotype.Component;

@Component
public class UserDAO {
	// Dummy database. Initialize with some dummy values.
	private static List<User> users;
	{
		users = new ArrayList();
		users.add(new User("test coding","123456","test@coding.test","0123456","aaa","aaa","12345","postman","adasdsdsds"));
	}
	
	public List list() {
		return users;
	}
	
	public User get(Long id) {
		for (User user : users) {
			if (user.getId()==id) {
				return user;
			}
		}
		return null;
	}
	
	public User create(User user) {
			users.add(user);
			return user;
	}
	
		public Long delete(Long id) {
		for (User user : users) {
			if (user.getId()==id) {
				users.remove(user);
				return id;
			}
		}
		return null;
		}
		
		public User update(Long id, User user) {
			for (User u : users) {
				if (u.getId()==id) {
					user.setId(u.getId());
					users.remove(u);
					users.add(user);
					return user;
				}
			}
				return null;
		}
		
}
