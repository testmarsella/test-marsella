package com.test.marsella.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.test.marsella.model.Shopping;
import com.test.marsella.model.User;

import org.springframework.stereotype.Component;

public class ShoppingDAO {
	private static List<Shopping> shoppings;
	{
		shoppings = new ArrayList();
		shoppings.add(new Shopping("new shopping",new Date(0)));
	}
	
	public List list() {
		return shoppings;
	}
	
	public Shopping get(Long id) {
		for (Shopping shopping : shoppings) {
			if (shopping.getId()==id) {
				return shopping;
			}
		}
		return null;
	}
	
	public Shopping create(Shopping shopping) {
			shoppings.add(shopping);
			return shopping;
	}
	
		public Long delete(Long id) {
		for (Shopping shopping : shoppings) {
			if (shopping.getId()==id) {
				shoppings.remove(shopping);
				return id;
			}
		}
		return null;
		}
		
		public Shopping update(Long id, Shopping shopping) {
			for (Shopping u : shoppings) {
				if (u.getId()==id) {
					shopping.setId(u.getId());
					shoppings.remove(u);
					shoppings.add(shopping);
					return shopping;
				}
			}
				return null;
		}
}
