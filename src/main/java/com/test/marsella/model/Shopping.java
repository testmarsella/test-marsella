package com.test.marsella.model;

import java.sql.Date;

public class Shopping {
	private long id;
	private String name;
	private Date createddate;
	public Shopping(String name, Date createddate) {
		super();
		this.id = id;
		this.name = name;
		this.createddate = createddate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}
	
	
}
