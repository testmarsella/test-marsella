package com.test.marsella.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.test.marsella.dao.ShoppingDAO;
import com.test.marsella.dao.UserDAO;
import com.test.marsella.model.Shopping;
import com.test.marsella.model.User;

public class ShoppingController {
	@Autowired
	private ShoppingDAO shoppingDAO;
	
	@GetMapping("/shoppings")
	public List getShoppings() {
		return shoppingDAO.list();
	}
	
	@GetMapping("/shoppings/{id}")
	public ResponseEntity getShopping(@PathVariable("id") Long id) {
		Shopping shopping = shoppingDAO.get(id);
		if (shopping == null) {
			return new ResponseEntity("No Data found for ID " + id,
			HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(shopping, HttpStatus.OK);
	}
	
	@PostMapping(value = "/create_shopping")
	public ResponseEntity createShopping(@RequestBody Shopping shopping) {
		shoppingDAO.create(shopping);
		return new ResponseEntity(shopping, HttpStatus.OK);
	}
	
	@DeleteMapping("/shopping_delete/{id}")
	public ResponseEntity deleteShopping(@PathVariable Long id) {
		if (null == shoppingDAO.delete(id)) {
			return new ResponseEntity("No Data found for ID " + id,
			HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(id, HttpStatus.OK);
	}
	
	@PutMapping("/shoppings_update/{id}")
	public ResponseEntity updateShopping(@PathVariable Long id, @RequestBody Shopping shopping) {
		shopping = shoppingDAO.update(id, shopping);
		if (null == shopping) {
			return new ResponseEntity("No Data found for ID " + id,
			HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity(shopping, HttpStatus.OK);
	}
}
